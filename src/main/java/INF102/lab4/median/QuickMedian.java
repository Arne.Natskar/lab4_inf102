package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {
    //Copy list
    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        int index = list.size()/2;
        return helpFunc(list, index);
    }
    private <T extends Comparable<T>> T helpFunc(List<T> list, Integer index){
        //System.out.println(list);
        if(list.size()==1){
            return list.get(0);
        }
        T pivot = list.get(0);
        List<T> lowerList = new ArrayList<T>();
        List<T> sameList = new ArrayList<T>();
        List<T> higherList = new ArrayList<T>();
        //lowerList, higherList = sortMe(list,pivot);
        for(int i = 0; i < list.size(); i++){
            //System.out.println(list.get(i) +". And pivot is: "+ pivot);
            T temp =list.get(i);
            if (temp.compareTo(pivot)==0){
                sameList.add(temp);
            }
            else if(temp.compareTo(pivot) == 1){ //Means get(i) is larger than pivot
                higherList.add(temp);
            }
            else if(temp.compareTo(pivot) == -1){
                lowerList.add(temp);
            }
            else{
                throw new IndexOutOfBoundsException("Something went wrong");
            }
        }
        List<T> myReturnlist = new ArrayList<T>();
        if (lowerList.size() > index){
            myReturnlist = lowerList;
        }
        else if((lowerList.size()+sameList.size()) > index){
            return sameList.get(0);
        }
        else{
            index = index - (lowerList.size()+ sameList.size());
            myReturnlist = higherList;
        }
        return helpFunc(myReturnlist,index);
    }
}
